\header {
  copyrightYear = "2010"
  composer = "Joseph Bodin de Boismortier"
  date = "1727"
}

#(set-global-staff-size 18)
\paper {
  #(define page-breaking ly:optimal-breaking)
}


\include "italiano.ly"
\include "nenuvar-lib.ily"

\paper {
  nenuvarBookTitleMarkup = \markup \when-property #'header:title \abs-fontsize #12 \column {
    \vspace#7
    \fill-line { \fontsize #6 \italic \fromproperty #'header:composer }
    \vspace#7
    \fontsize#9 \fill-line { \fromproperty #'header:title }
    \vspace#7
    \separation-line#0.2
    \vspace#7
    \fill-line { \fontsize #4 \fromproperty #'header:date }
    \vspace#4
    \fill-line { \fromproperty #'header:editions }
  }
  bookTitleMarkup = \nenuvarBookTitleMarkup
}

scene =
#(define-music-function (parser location title) (string?)
  (add-toc-item parser 'tocSceneMarkup (string-upper-case title))
  (add-odd-page-header-text parser (string-upper-case title) #t)
  (add-toplevel-markup parser #{\markup\scene $(string-upper-case title)#})
  (add-no-page-break parser)
  (make-music 'Music 'void #t))

opusTitle =
#(define-music-function (parser location title) (string?)
   (*opus-title* title)
   (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
   (make-music 'Music 'void #t))
