OUTPUT_DIR=out
DELIVERY_DIR=delivery
NENUVAR_LIB_PATH:=$(shell pwd)/../../nenuvar-lib
LILYPOND_OPTIONS=--loglevel=WARN -ddelete-intermediate-files -dno-protected-scheme-parsing
LILYPOND_CMD=lilypond -I$(shell pwd) -I$(NENUVAR_LIB_PATH) $(LILYPOND_OPTIONS)
PROJECT1=Boismortier - Suites à deux musettes opus 11
PROJECT2=Boismortier - Suites à deux musettes opus 17
###

opus11:
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT1)' opus11.ly
opus17:
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT2)' opus17.ly

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT1).pdf' ]; then mv -fv '$(OUTPUT_DIR)/$(PROJECT1).pdf' $(DELIVERY_DIR)/; fi
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT2).pdf' ]; then mv -fv '$(OUTPUT_DIR)/$(PROJECT2).pdf' $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT1)-1.midi ]; then tar zcf $(DELIVERY_DIR)/$(PROJECT1)-midi.tar.gz $(OUTPUT_DIR)/$(PROJECT1).midi $(OUTPUT_DIR)/$(PROJECT1)-[0-9]*.midi; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT2)-1.midi ]; then tar zcf $(DELIVERY_DIR)/$(PROJECT2)-midi.tar.gz $(OUTPUT_DIR)/$(PROJECT2).midi $(OUTPUT_DIR)/$(PROJECT2)-[0-9]*.midi; fi

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT1)* $(OUTPUT_DIR)/$(PROJECT2)*

all: check opus11 opus17 delivery clean

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi

.PHONY: parts opus11 opus17 clean all check delivery
