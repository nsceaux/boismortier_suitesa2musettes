\version "2.23.4"
\include "common.ily"
\opusTitle "Suites à deux musettes, opus 17"
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = \markup\center-column {
      \line { \concat { XVII \super e } Œuvre de \concat { M \super r } Boismortier }
      \fontsize#-2 \line { contenant VI suites à deux musettes }
      \fontsize#-4 \center-column {
        qui conviennent aux vieles, flûtes à bec, traversières, et hautbois
      }
    }
    poet = \markup\null
    editions = \markup\column {
      Source :
      \line { Joseph Bodin de Boismortier. \italic { XVIIe œuvre. } Paris: Boivin, 1727. }
      \line { \smaller
        \with-url #"http://numerique.bibliotheque.toulouse.fr/ark:/74899/B315556101_CONS0965_1"
        \typewriter "http://numerique.bibliotheque.toulouse.fr"
      }
      \line { Ref. bibl : Fonds musicaux anciens, Toulouse 300, RISM B3369 }
    }
  }
  %% title page
  \markup\null
  \pageBreak
  %% toc
  \markuplist
  \abs-fontsize-lines #8
  \with-line-width-ratio#0.7
  \override-lines #'(use-rehearsal-numbers . #f)
  \override-lines #'(column-number . 1)
  \table-of-contents
}
\bookpart {
  \scene "Première suite"
  \pieceToc "Allemande"          \includeScore "AAallemande"
  \pieceToc "Rigaugon"           \includeScore "ABrigaudon"
  \pieceToc "Gavotte en rondeau" \includeScore "ACgavotte"
  \pieceToc "Gigue"              \includeScore "ADgigue"
  \pieceToc "Sarabande"          \includeScore "AEsarabande"
  \pieceToc "Menuets"        \includeScore "AFmenuet" \includeScore "AGmenuet"
}

\bookpart {
  \scene "Deuxième suite"
  \pieceToc "Prélude"           \includeScore "BAprelude"
  \pieceToc "Gigue"             \includeScore "BBgigue"   \pageBreak
  \pieceToc "Rondeaux"      \includeScore "BCrondeau" \pageBreak
                                     \includeScore "BDrondeau" \pageBreak
  \pieceToc "Branle"            \includeScore "BEbranle"
  \pieceToc "Rigaudons"     \includeScore "BFrigaudon" \includeScore "BGrigaudon"
}
\bookpart {
  \scene "Troisième suite"
  \pieceToc "Rondeau"      \includeScore "CArondeau"
  %{ \pieceToc "?" %}      \includeScore "CB"
  \pieceToc "Chaconne"     \includeScore "CCchaconne"
  \pieceToc "Bourrée"      \includeScore "CDbourree" \pageBreak
  \pieceToc "Sarabande"    \includeScore "CEsarabande"
  \pieceToc "Gavottes" \includeScore "CFgavotte" \includeScore "CGgavotte"
}
\bookpart {
  \scene "Quatrième suite"
  \pieceToc "Allemande"    \includeScore "DAallemande"
  \pieceToc "Paysanne"     \includeScore "DBpaysanne" \pageBreak
  \pieceToc "Rondeau"      \includeScore "DCrondeau"
  \pieceToc "Loure"        \includeScore "DDloure"
  \pieceToc "Menuet"       \includeScore "DEmenuet"   \pageBreak
  \pieceToc "Gavottes" \includeScore "DFgavotte" \includeScore "DGgavotte"
}
\bookpart {
  \scene "Cinquième suite"
  \pieceToc "Prélude"        \includeScore "EAprelude"
  \pieceToc "Marche"         \includeScore "EBmarche"
  %{\pieceToc "?" %}         \includeScore "EC"        \pageBreak
  \pieceToc "Rondeaux"   \includeScore "EDrondeau" \pageBreak
                                  \includeScore "EErondeau" \pageBreak
  \pieceToc "Sarabande"      \includeScore "EFsarabande"
  \pieceToc "Passepieds" \includeScore "EGpassepied" \includeScore "EHpassepied"
}
\bookpart {
  \scene "Sixième suite"
  \pieceToc "Prélude"     \includeScore "FAprelude"
  \pieceToc "Allemande"   \includeScore "FBallemande" \pageBreak
  \pieceToc "Rondeau"     \includeScore "FCrondeau"   \pageBreak
  \pieceToc "Canaries"    \includeScore "FDcanaries"
  \pieceToc "Courante"    \includeScore "FEcourante"  \pageBreak
  \pieceToc "Sarabande"   \includeScore "FFsarabande"
  \pieceToc "Menuets" \includeScore "FGmenuet" \includeScore "FHmenuet"
}
