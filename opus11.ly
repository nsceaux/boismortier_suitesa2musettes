\version "2.23.4"
\include "common.ily"
\opusTitle "Suites à deux musettes, opus 11"
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = \markup\center-column {
      \line { \concat { XI \super e } Œuvre de \concat { M \super r } Boismortier }
      \fontsize#-2 \line { contenant VI suites à deux musettes }
      \fontsize#-4 \center-column {
        qui conviennent aux vieles, flûtes à bec, traversières, et hautbois
      }
    }
    poet = \markup\null
    editions = \markup\column {
      Source :
      \line { Joseph Bodin de Boismortier. \italic { XIe œuvre. } Paris: Boivin, 1727. }
      \line { \smaller
        \with-url #"http://numerique.bibliotheque.toulouse.fr/ark:/74899/B315556101_CONS0965_2"
        \typewriter "http://numerique.bibliotheque.toulouse.fr"
      }
      \line { Ref. bibl : Fonds musicaux anciens, Toulouse 299, RISM B3362 }
    }
  }
  %% title page
  \markup\null
  \pageBreak
  %% toc
  \markuplist
  \abs-fontsize-lines #8
  \with-line-width-ratio#0.7
  \override-lines #'(use-rehearsal-numbers . #f)
  \override-lines #'(column-number . 1)
  \table-of-contents
}

\bookpart {
  \scene "Première suite"
  \pieceToc "Ouverture"        \includeScore "GAouverture" \pageBreak
  \pieceToc "Musette"          \includeScore "GBmusette"   \pageBreak
  \pieceToc "Vielle"           \includeScore "GCvielle"
  \pieceToc "Menuet"           \includeScore "GDmenuet"    \pageBreak
  \pieceToc "Sarabande"        \includeScore "GEsarabande"
  \pieceToc "Contredanse I/II" \includeScore "GFcontredanse" \includeScore "GHcontredanse"
}
\bookpart {
  \scene "Deuxième suite"
  \pieceToc "Prélude"     \includeScore "HAprelude"
  \pieceToc "Rondeau"     \includeScore "HBrondeau" \pageBreak
  \pieceToc "Passacaille" \includeScore "HCpassacaille"
  \pieceToc "Bourrée"     \includeScore "HDbourree"
  \pieceToc "Brunette"    \includeScore "HEbrunette"
  \pieceToc "Menuet I/II" \includeScore "HFmenuet" \includeScore "HGmenuet"
}
\bookpart {
  \scene "Troisième suite"
  \pieceToc "Allemande"     \includeScore "IAallemande"
  \pieceToc "Fanfare"       \includeScore "IBfanfare" \pageBreak
  \pieceToc "Menuet"        \includeScore "ICmenuet"
  \pieceToc "Courante"      \includeScore "IDcourante"
  \pieceToc "Rondeau"       \includeScore "IErondeau"
  \pieceToc "Pavanne"       \includeScore "IFpavanne" \pageBreak
  %{ \pieceToc "?" %}       \includeScore "IG"
  \pieceToc "Rigaudon I/II" \includeScore "IHrigaudon" \includeScore "IIrigaudon"
}
\bookpart {
  \scene "Quatrième suite"
  \pieceToc "Prélude"      \includeScore "JAprelude"
  \pieceToc "Paysanne"     \includeScore "JBpaysanne" \pageBreak
  \pieceToc "Rondeau I/II" \includeScore "JCrondeau"
  \includeScore "JDrondeau" \pageBreak
  %{ \pieceToc "?" %}      \includeScore "JE"
  \pieceToc "Gavotte I/II" \includeScore "JFgavotte" \includeScore "JGgavotte"
}
\bookpart {
  \scene "Cinquième suite"
  \pieceToc "Allemande"         \includeScore "KAallemande"
  \pieceToc "Marche des Oberés" \includeScore "KBmarche" \pageBreak
  \pieceToc "Musette"           \includeScore "KCmusette"
  \pieceToc "Gavotte"           \includeScore "KDgavotte"
  %{ \pieceToc "?" %}           \includeScore "KE"
  %{ \pieceToc "?" %}           \includeScore "KF" \pageBreak
  \pieceToc "Sarabande"         \includeScore "KGsarabande"
  \pieceToc "Passepied I/II"    \includeScore "KHpassepied" \includeScore "KIpassepied"
}
\bookpart {
  \scene "Sixième suite"
  \pieceToc "Prélude"     \includeScore "LAprelude"
  \pieceToc "Rondeau"     \includeScore "LBrondeau" \pageBreak
  \pieceToc "Allemande"   \includeScore "LCallemande"
  \pieceToc "Villageoise" \includeScore "LDvillageoise"
  \pieceToc "Branle"      \includeScore "LEbranle"
  \pieceToc "Menuet"      \includeScore "LFmenuet"
  \pieceToc "Brunette"    \includeScore "LGbrunette" \pageBreak
  \pieceToc "Gigue I/II"  \includeScore "LHgigue" \includeScore "LIgigue"
  \actEnd "FIN"
}
